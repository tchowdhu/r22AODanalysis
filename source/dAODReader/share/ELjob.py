#!/usr/bin/env python                                                                                    

import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'test',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.                                                                                       
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro                                      
# for the details about these lines.                                                                     
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

#art
#inputFilePath="/eos/atlas/user/l/longjon/Trigger/AODs/"
#ROOT.SH.ScanDir().filePattern( 'AOD.art.pool.root' ).scan( sh, inputFilePath )
inputFilePath="/eos/home-t/tchowdhu/derivation/daodreader/run/"
#ROOT.SH.ScanDir().filePattern( 'AOD.pool_p.root' ).scan( sh, inputFilePath )
ROOT.SH.ScanDir().filePattern( 'DAOD_PHYSVAL.pool.root' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.                                                                               
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100)
#job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))


#from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
#alg = AnaAlgorithmConfig( 'MyxAODAnalysis/AnalysisAlg' )

# Create the algorithm's configuration.                                                                  
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'dAODReader', 'AnalysisAlg' )

# Setup TDT
#from AnaAlgorithm.DualUseConfig import addPrivateTool
#from AnaAlgorithm.DualUseConfig import createPublicTool
#addPrivateTool(alg, 'm_configTool', "TrigConf::xAODConfigTool")

#addPrivateTool(alg, 'm_trigDec', "Trig::TrigDecisionTool")
#addPrivateTool(alg, "m_trigDec.ConfigTool", "TrigConf::xAODConfigTool")

#alg.m_trigDec.ConfigTool = alg.m_configTool
#alg.m_trigDec.TrigDecisionKey = "xTrigDecision"
#alg.m_trigDec.NavigationFormat = "TrigComposite"


#from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
#alg = AnaAlgorithmConfig( 'dAODReader/AnalysisAlg')
                         
# Add our algorithm to the job                                                                           
job.algsAdd( alg ) ##

sh_hist = ROOT.SH.SampleHandler()
#sh_hist.load (options.submission_dir + '/output-ANALYSIS')


# Run the job using the direct driver.                                                                   
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
sh_hist = ROOT.SH.SampleHandler()
sh_hist.load (options.submission_dir + '/hist')

#sh_hist.load (options.submission_dir + '/output-ANALYSIS')

