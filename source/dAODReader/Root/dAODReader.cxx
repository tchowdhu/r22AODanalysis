#define _USE_MATH_DEFINES

#include <AsgTools/MessageCheckAsgTools.h>
#include <dAODReader/dAODReader.h>
#include <algorithm> 

dAODReader :: dAODReader (const std::string& name, ISvcLocator *pSvcLocator): EL::AnaAlgorithm (name, pSvcLocator)
{
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  This is also where you
    // declare all properties for your algorithm.  Note that things like
    // resetting statistics variables or booking histograms should
    // rather go into the initialize() function.
    //declareProperty( " ",  , "     " );
}


dAODReader :: ~dAODReader()
{
  delete m_jetEta;
  delete m_jetPhi;
  delete m_jetPt;
  delete m_jetE;
  delete m_c_clamda;
  delete m_c_rawEta;
  delete m_time;
}


StatusCode dAODReader :: initialize()
{
    
    ANA_CHECK (book (TTree ("analysis", "My analysis ntuple")));
    TTree* mytree = tree ("analysis");
    // Add Event info 
    mytree->Branch ("RunNumber", &m_runNumber);
    mytree->Branch ("EventNumber", &m_eventNumber);
    
    
    m_jetEta = new std::vector<float>(); 
    m_jetPhi = new std::vector<float>();
    m_jetPt  = new std::vector<float>();
    m_jetE   = new std::vector<float>();
    
    m_c_clamda= new std::vector<float>();
    m_c_rawEta= new std::vector<float>();
    m_time    = new std::vector<float>();
    
    mytree->Branch ("JetEta", &m_jetEta);
    mytree->Branch ("JetPhi", &m_jetPhi);
    mytree->Branch ("JetPt", &m_jetPt);
    mytree->Branch ("JetE", &m_jetE);
    
    mytree->Branch ("TopoCluster_CenterLamda", &m_c_clamda);
    mytree->Branch("TopoCluster_RawEta",       &m_c_rawEta);
    //mytree->Branch("TopoCluster_SecondTime",       &m_c_stime);
    mytree->Branch("TopoCluster_Time",              &m_time);
    //mytree->Branch("GhostTracks",                   &m_gTracks);
    
  
    
    //book histograms
    ANA_CHECK (book (TH1F ("h_jetPt", "h_jetPt", 100, 0, 500))); // jet pt [GeV]
    ANA_CHECK (book (TH1F ("h_CaloCalTopoCluster_SecondTime", "h_CaloCalTopoCluster_SecondTime", 100, 0, 500))); // jet pt [GeV]
    ANA_CHECK (book (TH1F ("h_GhostTower", "h_GhostTower", 100, 0, 100))); // jet pt [GeV]
    

    return StatusCode::SUCCESS;
}

StatusCode dAODReader :: execute()
{

    // Just try printing some event info
    ANA_CHECK (evtStore()->retrieve (ei, "EventInfo"));
    ANA_MSG_INFO ("in execute, runNumber = " << ei->runNumber() << ", eventNumber = " << ei->eventNumber());
    m_runNumber = ei->runNumber ();
    m_eventNumber = ei->eventNumber ();
    
    // Read Moments from a specific container
    const xAOD::CaloClusterContainer * m_clusters = 0;
    ANA_CHECK(evtStore()->retrieve( m_clusters, "CaloCalTopoClusters"));//CaloCalTopoTowers
    
    m_c_clamda->clear();
    m_time->clear();
    m_c_rawEta->clear();
    
    for(auto i_t : *m_clusters){
    m_c_rawEta-> push_back(i_t->rawEta());
    m_time-> push_back(i_t->time());
    m_c_clamda-> push_back(i_t->getMomentValue(xAOD::CaloCluster::MomentType::CENTER_LAMBDA));
    //m_c_stime=i_t->getMomentValue(xAOD::CaloCluster::MomentType::SECOND_TIME);
    }
    
 
    /** dAOD Jets **/
    //Read/fill the jet variables:
    const xAOD::JetContainer* jets = nullptr;
    ANA_CHECK (evtStore()->retrieve (jets, "AntiKt4EMPFlowFEJets"));
    m_jetEta->clear();
    m_jetPhi->clear();
    m_jetPt->clear();
    m_jetE->clear();
    //const xAOD::Jet* fatjet_parent = fatjet;
    for (const xAOD::Jet* jet : *jets) {
        m_jetEta->push_back (jet->eta ());
        m_jetPhi->push_back (jet->phi ());
        m_jetPt-> push_back (jet->pt ());
        m_jetE->  push_back (jet->e ());
        //m_testPt = jet->pt ();
        hist ("h_jetPt")->Fill (jet->pt() * 0.001); // GeV
        
        //const xAOD::Jet* parent = jet->getAssociatedObject<xAOD::Jet>("Parent");

        std::cout<<"Jet (pt,m): pt="<<jet->pt()/1000.<<"  m="<<jet->m()/1000.<<std::endl;
                                  //<<"  tracks="<<assotrkjets
                                  
                                  
                                 
        std::vector<const xAOD::Jet*> tracks = jet->getAssociatedObjects<xAOD::Jet>("GhostTowers");

        }//// end for loop over jets
        
    // Fill the event into the tree:
    tree ("analysis")->Fill ();
    
    

    return StatusCode::SUCCESS;
}

StatusCode dAODReader :: finalize()
{
    return StatusCode::SUCCESS;
}






