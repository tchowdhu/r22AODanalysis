#ifndef DAODREADER_H
#define DAODREADER_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/ToolHandle.h>

#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include <xAODJet/JetContainer.h>
#include <xAODJet/JetAuxContainer.h>

//#include "TrigConfxAOD/xAODConfigTool.h"
//#include "TrigDecisionTool/TrigDecisionTool.h"
//#include "xAODTrigger/TrigCompositeContainer.h"


// root includes
#include "TH1D.h"
#include "TH2D.h"
#include <TLorentzVector.h>
#include "TGraphAsymmErrors.h"
#include <TTree.h>
#include <vector>


class dAODReader : public EL::AnaAlgorithm
{
 public:
    // this is a standard algorithm constructor
    dAODReader (const std::string& name, ISvcLocator* pSvcLocator);
    ~dAODReader();

    // these are the functions inherited from Algorithm
    virtual StatusCode initialize () override;
    virtual StatusCode execute () override;
    virtual StatusCode finalize () override;

 private:
   // Configuration, and any other types of variables go here.
   const xAOD::EventInfo* ei;
   unsigned int m_runNumber = 0; ///< Run number
   unsigned long long m_eventNumber = 0; ///< Event number
   
   
   /// Jet 4-momentum variables
   std::vector<float> *m_jetEta = nullptr;
   std::vector<float> *m_jetPhi = nullptr;
   std::vector<float> *m_jetPt = nullptr;
   std::vector<float> *m_jetE = nullptr;
   
  
   std::vector<float> *m_c_clamda = nullptr;
   std::vector<float> *m_time = nullptr; 
   std::vector<float> *m_c_rawEta = nullptr;
   
   


};

#endif
